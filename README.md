# Practical Scientific Computing
## BIOL 4800
## Fall 2019
## Dr. Michael Brewer

## 1 -- Regular Expression

Much of our time as scientists is spent rearranging and reformatting text files. Regular expression (i.e., RegEx) is a language that allow powerful and customizable find and replace within text files. The language is built into many text editors and other programming languages. The following examples will demonstrate the utility of RegEx while introducing the basics of syntax and logic underlying much of scientific computing.

### **2018_08_20**

### Agalma elegans example

"Agalma elegans"

Replace "galma" with "."

Try with more taxa:
	"Frillagalma vityaxi"
	"Cordagalma tottoni"

"Mus musculus" --> M. m.cul.

_*This is why we use wildcards!*_

#### "\w" example and capturing text

Matches all numbers [0-9], letters [A-z], and "\_"

+40 46'N +014 15'E
+21 17'N -157 52'W

If you find and replace "\w" with "", it leaves only +, -, and '.
	However, find and replace "'\w" with "", and you remove only N, E, and W.

Back to scientific names:

Tetragnatha versicolor Walckenaer, 1841
Pachygnatha clerkoides Wunderlich, 1985
Dolichognatha albida (Simon, 1895)

Agalma elegans
Frillagalma vityaxi
Cordagalma tottoni

```
(\w)\w+ (\w+) -> $1. $2
```

#### Make a tab-delimited translation table

```
(\w)(\w+) (\w+) -> $1$2 $3\t$1_$3
```

#### Put sample names first

```
(\w)(\w+) (\w+) -> $1_$3\t$1$2 $3
```

#### RegEx Wildcards:
	\w	A-Z, a-z, and 0-9 and _
	\d	0-9
	\n	Mac and Linux linebreak
	\r\n	Windows linebreak
	\s	spaces, tabs, and linebreaks
	\t	tab
	[AGCT]	Character set -> Only A, T, C, and G
	[^\t]	negation -> anything other than a tab
	^	Boundary -> beginning of line
	$	Boundary -> end of line
	?	Minimum match -> minimum number of characters to fit pattern
	\*	Zero or more matches to fit pattern
	{}	Control number of matches

#### With taxonomy

Agalma elegans (Sars, 1846)
Frillagalma vityaxi Daniel, 1966
Cordagalma tottoni Margulis, 1993

```
( ->
) ->
(\w)(\w+) (\w+) (\w+), (\d+) -> $1_$3\t$1$2 $3\t$4\t$5
```

### GenBank example

\>CAA58790.1= GFP [Aequorea victoria]\
MMSDLSDFOINVLKSDVNOIWENGLKWENVPWENVPWNEKLSDLVKNWPENG\
\>AAZ67342.1= GFP-like red flourescent protein [Corynactis californica]\
SLDKVNIPENVPNOVNOWEBNOWBVPONVOENVOIWINEVONBOWNBOIWNB

_*Reduce headers*_

#### Accession genus header
```
(>\w+).+\[(\w+).+ -> $1_$2
```

#### G-species header
```
(>\w+).+\[(\w)\w+ (\w+).+ -> $1_$2-$3
```

#### Database Accession G_species seq
```
>(\w+).+\[(\w)\w+ (\w+).+\n -> $1\t$2_$3\t
```

### Custom wildcards using []

#### Lat-Lon example

21 17'24.68"N
157 51'41.50"W
38 30'36.62"N
28 17'16.87"W
8 59'53.30"S
157 58'13.70"W
10 24'47.84"N
51 21'54.61"E
22 52'41.65"S
48 9'46.62"E

```
(\"[NS])\n -> $1\t
([0-9]+ [0-9 \'\"\.]+)[WS] -> -$1
[NE] ->
```

#### Remove Eastern Hemisphere Data
```
.+\t([^-]+) ->
```

#### Remove Southern hemisphere Data
```
^[-].+ ->
```

#### Poly-A tail example

ACGCTAGCTACGCTACGCTCTCGACTCGACTAGCCTCGACTCGATCGACTAAAAAAAAAAAAAAAAAAAAAAAAA

```
(\w+?)A*$ -> $1
```

### Field Notes example

Field notes  
18-vii-2014  
13 January, 1752 at 13:53	-1.414	 5.781	 Found in tide pools	 20C  
17 March, 1961 at 03:46	 14	 3.6	 Thirty specimens observed	 18C  
1 Oct., 2002 at 18:22	 36.51	 -3.4221	 Genome sequenced to confirm	 21C  
20 July, 1863 at 12:02	 1.74	 133	 Article in Harper's	 16C  

Want:
Just full month name, no punctuation

```
\d+\s+([A-Za-z]+)[\w\,\.].+ -> $1
```

_Undo changes_

### **_HOMEWORK_**
#### Match boundaries, quantifiers, and rearranging columns of a tab-delimited file

Want:
Year	Mon.	Day	Hour	Minute	xdata	ydata	temp

```
(\d+)\s+(\w{3})[\w\,\.]+\s+(\d+)\s+at\s+(\d+):(\d+)\s+([\d\.-]+)\s+([\d\.-]+)\s+.+\s+(\d+)C$ -> $3\t$2.\t$1\t$4\t$5\t$6\t$7\t$8
```

Want:
Temperature before text notes.

```
(\d+)\s+(\w{3})[\w\,\.]+\s+(\d+)\s+at\s+(\d+):(\d+)\s+([\d\.-]+)\s+([\d\.-]+)\s+(.+)\s+(\d+)C$ -> $3\t$2.\t$1\t$4\t$5\t$6\t$7\t$9\t$8
```

## 2 -- Unix command line and BASH

### **2019_08_22**

#### OS setup

*_Windows 10/Linux_*

Install Ubuntu from Windows Store

```
sudo apt-get update
sudo apt-get install vim

sh -c "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install.sh)"

test -d ~/.linuxbrew && PATH="$HOME/.linuxbrew/bin:$HOME/.linuxbrew/sbin:$PATH"
test -d /home/linuxbrew/.linuxbrew && PATH="/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin:$PATH"
test -r ~/.bash_profile && echo "export PATH='$(brew --prefix)/bin:$(brew --prefix)/sbin'":'"$PATH"' >>~/.bash_profile
echo "export PATH='$(brew --prefix)/bin:$(brew --prefix)/sbin'":'"$PATH"' >>~/.profile

brew install hello
```

*_Mac_*

Install Command line tools from App Store and Homebrew

```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

*_Download pcfb sample data:_*

```
curl http://practicalcomputing.org/files/pcfb_examples.zip -o pcfb_examples.zip
```

#### Getting started with the shell and changing default shell

```
echo $SHELL

>chsh -s /bin/bash
log out and log back in
```

#### Path

Absolute path vs. relative path vs. working directory

#### Navigating via the shell

```
ls
ls Desktop

pwd

cd Desktop
ls Desktop
```
What happened?

```
ls /Desktop
ls /home/manager/Desktop
```

Why does this work?

```
cd ..
ls
cd Desktop
cd ../Documents

cd ~/Desktop

ls
mkdir test
ls
```

Look at graphical Desktop

```
cd test
ls
cd ..
rmdir test

mkdir test
cd test
touch meh.txt
ls
cd ..
rmdir test
```

Why didn't this work?

```
cd test
rm meh.txt
cd ..
rmdir test
ls

mkdir test
touch meh.txt
cp meh.txt test/
rm test/meh.txt
cp meh.txt test/meh-dup.txt
rm test/meh-dup.txt

mv meh.txt meh_renamed.txt
mv meh_renamed.txt test/meh.txt
ls
ls test/
```

*Shortcuts up arrow and tab*

```
cd ~/Doc [tab]
cd ~/D [tab]
```

Why doesn't this work?

```
cd ~/D [tab][tab]
```

*Special characters*

Create a directory on the Desktop with a space "My Project"

```
cd ~/Desktop
>cd My Project
```

Why didn't this work?

```
cd My [tab]
```

Use escape character!

```
cd ..
cd My\ Project
```

#### Modifying commands with arguments


```
cd
```

Why did this take us home?

```
ls
ls -a
```

How is this different?

```
ls -l
```

How is this different?

```
ls -la
man ls
```

### **2019_08_27**

#### Viewing text with less

```
cd Desktop
```

*Copy pcfb_examples.zip to Desktop*

```
unzip pcfb_examples.zip
cd pcfb
ls
cd examples
ls
ls *.seq
less FEC00001_1.seq
```

#### Copying/moving/deleting multiple files

```
cp *.seq ~/Desktop
ls ~/Desktop/
rm ~/Desktop/*.seq
ls ~/Desktop/
cd ~/Desktop
cp pcfb/examples/*.seq ./
rm *.seq
```
#### Directing output

```
cd ~/Desktop/pcfb/sandbox/
ls -l ../examples/*.seq
ls -l ../examples/*.seq > files.txt

cat ../examples/*.seq
cat ../examples/*.seq > sequences.fasta
cat ../examples/*.seq >> sequences.fasta
```

What happeded?

```
cat ../examples/*.seq > sequences.fasta
```

How is this different?

#### Counting with grep

```
grep "Fe" sequences.fasta
grep ">" sequences.fasta
grep -c "^>" sequences.fasta
```

#### Using grep

```
cd ~/Desktop/pcfb/sandbox/
grep "Toolik Lake" ../examples/shaver_etal.csv
grep "Toolik Lake" ../examples/shaver_etal.csv > toolik.csv
grep "toolik lake" ../examples/shaver_etal.csv
grep -i "toolik lake" ../examples/shaver_etal.csv
grep "Aug.*Toolik Lake" ../examples/shaver_etal.csv > toolik2.csv
grep -v "Toolik Lake" ../examples/shaver_etal.csv
grep -c "Toolik Lake" ../examples/shaver_etal.csv
```

#### Piping & history command

```
history
history | grep Toolik
grep "Aug" ../examples/shaver_etal.csv | grep "Toolik"
grep "Aug" ../examples/shaver_etal.csv | grep "Toolik" > toola2.csv

cd ~/Desktop/pcfb/examples/
grep "^>" *.seq
```

Is this useful?

```
cat *.seq
cat *.seq | grep "^>"
```

What is the difference?

```
grep -l "GAATTC" *.seq
grep -l "GAATTC" *.seq > ../sandbox/has_EcoRI.txt
```

#### Curl

```
curl "http://files.rcsb.org/view/4hhb.pdb"
curl "http://files.rcsb.org/view/4hhb.pdb" > 4hhb.pdb
curl "http://files.rcsb.org/view/4hhb.pdb" -o 4hhb.pdb

curl "http://files.rcsb.org/view/[1-4]hhb.pdb" -o "#1hhb.pdb"
curl "http://files.rcsb.org/view/4hhb.{sif,xml}" -o "4hhb.#1"
curl "http://files.rcsb.org/view/[1-4]hhb.{pdb,sif,xml}" -o "#1hhb.#2"
```

#### Path stuff

```
which cd
which ls
which bash
which which
```

#### Creating workspace

```
cd
mkdir scripts
ls
echo $PATH
set
echo $USER
echo "Hi, I am $USER."

nano ~/.profile
export PATH="$PATH:${HOME}/scripts"
source ~/.profile
```

### **2019-08-29**

#### Scripting!!!

*Shebang*

```
nano
```

\#! /bin/bash

```
ls -la
echo "Above are the directory listings for this folder:"
pwd
echo "Right now it is:"
date
```

[ctrl+x] and save and name dir.sh

```
dir.sh
ls -l
chmod u+x dir.sh
ls -l
dir.sh
cd ~/Desktop
dir.sh
```

#### Ending the terminal session

```
exit
```

#### Using grep and cp to make a bulk file copying script

```
cd ~/Desktop/praccomp/pcfb/examples
grep -li fluor *.pdb
grep -li fluor *.pdb > ~/praccomp/scripts/copier.sh
nano ~/praccomp/scripts/copier.sh
```
Linux:
```
alt+r
alt+r
```

Mac:
```
ctrl+W
esc+r
```

```
^(.+) -> cp ~/Desktop/praccomp/pcfb/examples/\1 ~/Desktop/pcfb/praccomp/sandbox/
```

#### add shebang

```
chmod u+x ~/praccomp/scripts/copier.sh
copier.sh
```

#### Aliases

```
ls
ls -la
alias la="ls -la"
add to ~/.profile
```

#### Advanced Unix commands

```
cd ~/Desktop/pcfb/examples
head ThalassocalyceData.txt
tail ThalassocalyceData.txt
head -n 2 ThalassocalyceData.txt
head -n 5 ThalassocalyceData.txt
tail -n 5 ThalassocalyceData.txt

cut -f 2-4 ~/Desktop/pcfb/examples/ThalassocalyceData.txt
head -n 20 ~/Desktop/pcfb/examples/ctd.txt | less -S
head -n 20 ~/Desktop/pcfb/examples/ctd.txt | cut -d "," -f 1,5,7,9
grep ">" ~/Desktop/pcfb/examples/FPexamples.fta
grep ">" ~/Desktop/pcfb/examples/FPexamples.fta | cut -c 2-11
grep ">" ~/Desktop/pcfb/examples/FPexcerpt.fta | sort
grep ">" ~/Desktop/pcfb/examples/FPexcerpt.fta | sort | cut -c 2-
tail -n +2 ~/Desktop/pcfb/examples/ThalassocalyceData.txt | sort -k 2
tail -n +2 ~/Desktop/pcfb/examples/ThalassocalyceData.txt | sort -k 2 -n
```

### Big example

```
grep ATOM structure_1gfl.pdb
grep ATOM structure_1gfl.pdb | grep -v REMARK
grep ATOM structure_1gfl.pdb | grep -v REMARK | cut -c 18-21,24-26
grep ATOM structure_1gfl.pdb | grep -v REMARK | cut -c 18-21,24-26 | sort
grep ATOM structure_1gfl.pdb | grep -v REMARK | cut -c 18-21,24-26 | sort | uniq
grep ATOM structure_1gfl.pdb | grep -v REMARK | cut -c 18-21,24-26 | sort | uniq | cut -f 1 -d " "
grep ATOM structure_1gfl.pdb | grep -v REMARK | cut -c 18-21,24-26 | sort | uniq | cut -f 1 -d " " | uniq -c
grep ATOM structure_1gfl.pdb | grep -v REMARK | cut -c 18-21,24-26 | sort | uniq | cut -f 1 -d " " | uniq -c | sort -nr
```

### Group activity

#curl "https://www.dropbox.com/s/w1a4wrf3g8gntbj/Brachycybe_lecontii_sequence.fasta?dl=0"
#curl "https://www.dropbox.com/s/w1a4wrf3g8gntbj/Brachycybe_lecontii_sequence.fasta?dl=0" -o Brachycybe_lecontii_sequence.fasta
```
curl -L "https://www.bit.ly/Brac_mtSeqs" -o Brachycybe_lecontii_sequence.fasta
```

**Number of samples from each state**
```
grep "^>" Brachycybe_lecontii_sequence.fasta | grep -v "complete genome" | cut -d " " -f 5 | cut -c 1-2 | sort | uniq -c
```

**Number of each gene**
```
grep "^>" Brachycybe_lecontii_sequence.fasta | grep -v "complete genome" | cut -d "(" -f 2 | cut -d ")" -f 1 | sort | uniq -c
```

### **2019-09-03**

## 4 - R Statistical Computing

```
cd Desktop/pcfb/sandbox
wget -O RTutorialData.zip https://bit.ly/r-spiders
unzip RTutorialData.zip
```

### I. Introduction
#### A. Downloading & Installing R
1. http://www.r-project.org

2. Launch installer and follow directions

3. http://www.rstudio.com

4. Launch installer and follow directions

#### B. Interface

1. Commands are typed after the ">"

2. Using R as a calculator

EXAMPLE:

```
>2 + 2
>2^2
>(1 - 2) * 3
>sqrt(9)
>log(10)
```

3. Error messages

EXAMPLE:

```
>squareroot(2)
>sqrt 2
>sqrt(-2)
```

#### C. Installing and Running Packages
1. Installing Packages
a. Using "install.packages"

EXAMPLE:

```
>install.packages("vegan", dependencies = TRUE)
```

b. Using "Package Installer" --> Also used to update packages

EXAMPLE: Packages & Data -> Package Installer
Choose package
Check "install dependencies"
Click "Install Selected"

2. Calling Packages
a. Using "library()"

EXAMPLE:

```
>library(vegan)
```

b. Using "Package Manager"
EXAMPLE:Packages & Data -> Package Manager
Check box next to desired package

#### D. Getting Help
1. Explains arguments for a command
EXAMPLE: >help(lm)

2. Sometimes confusing --> google search is always helpful

#### E. Object Oriented Languages
1. Define objects (i.e. Variables, Data, Transformations, etc.)

EXAMPLE:

```
>object1 <- 2+2
>object1 + object1
>sqrt.ob1 <- sqrt(object1)
```

2. Objects saved in memory and are of certain "classes" depending on how they were entered or derived

3. Analyses can be assigned as objects so the output may be used in subsequent analyses

### II. Data Entry and Manipulations
#### A. Manual Data Entry
1. Using c()
EXAMPLE:

```
>rand <- c(12, 54, 98, 65, 38)
>rand
>sum(rand)
>length(rand)
>avg.rand <- sum(rand)/length(rand)
>avg.rand
>sort(rand)
>min(rand)
>max(rand)
>cumsum(rand)
>diff(rand)
>rand[2]
>rand*2
```

2. Using scan()

EXAMPLE:

```
>pedes <- scan()
1: 2 3 16 23 14 12 4 13 2 0 0 0 6 28 31 14 4 8 2 5
21:
>pedes
```

#### B. Reading & Importing Files

1. Using read.csv() [or read.delim()]

EXAMPLE:

```
>example <- read.csv("PATH/file.csv") #PCs need to be in the correct working directory
>example
>labels(example)
```

2. From the internet

EXAMPLE:

```
>whale.data <- read.csv(file="https://sakai.unc.edu/access/content/group/3d1eb92e-7848-4f55-90c3-7c72a54e7e43/public/data/bycatch.csv", header=TRUE)
>whale.data
```

#### C. Editing data

1. Edit text file or spreadsheet and reload

2. Edit in R

EXAMPLE:

```
>edit(whale.data)
```

*Can make new variables and transformations, but no real need as we will see later*

### **2019_09_10**

**Discussion of statistics**
- Population vs. Sample
- Measures of Center
- Variation
- Distributions
- Tests
	- Z-Scores
	- Chi-squared Tests
	- T Tests

_Readings_
Krzyminski and Altman 2013 Nat. Methods 10, 809-810
Broman and Woo 2018 The American Statistican 72(1), 2-10

### **2019_09_17**

_Reviewed previous R material in Rstudio and created RMarkdown files_

### III. Analyses

#### A. Univariate Statistics

1. Categorical Data

a. Barplots

EXAMPLE:

```
>beer <- c(3,4,1,1,3,4,3,3,1,3,2,1,2,1,2,3,2,3,1,1,1,1,4,3,1)
>barplot(beer) #WRONG
>barplot(table(beer), xlab="Beer", ylab="Frequency") #Counts
>barplot(table(beer)/length(beer), xlab="Beer", ylab="Proportion") #Proportions
```

b. Pie Charts

EXAMPLE:

```
>pie(table(beer), main="Beer")
```

2. Numerical Data

a. Stem-and-leaf Plots

EXAMPLE:

```
>stem(pedes)
```

b. Strip Chart

EXAMPLE:

```
>stripchart(pedes, method="stack")
```

3. Measures of Center

a. Mean

EXAMPLE:

```
>mean(pedes)
>mean(whale.data)
```

b. Median

EXAMPLE:

```
>median(pedes)
>median(whale.data) #FAIL
>with(whale.data, median(texas))
>with(whale.data, median(florida))
```

c. Mode

EXAMPLE:

```
>which(table(pedes) == max(table(pedes)))
>with(whale.data, which(table(florida) == max(table(florida))))
>with(whale.data, which(table(texas) == max(table(texas))))
```

4. Variation

a. Range

EXAMPLE:

```
>range(pedes)
>diff(range(pedes))
```

b. Variance

EXAMPLE:

```
>var(pedes)
>sd(pedes)
```

c. IQRs

```
>IQR(pedes)
```

d. Z-Scores

EXAMPLE:

```
>scale(pedes)
```

e. Summary

EXAMPLE:

```
>summary(pedes)
```

5. Plots

a. Histograms

EXAMPLE:

```
>hist(pedes)
>hist(whale.data)#FAIL
>hist(pedes, breaks=2)
>hist(pedes, breaks="scott")
>hist(pedes, prob=TRUE)#y-axis = proportions
>lines(density(pedes))
>plot(density(pedes))
```

b. Box Plots

EXAMPLE:

```
>boxplot(pedes)
>summary(boxplot(pedes))
```

### 2019_09_19

#### B. Bivariate & Multivariate Statistics
1. Plotting and Regression

a. Box Plotting

EXAMPLE:

```
>spid.gen
>boxplot(spid.gen)#FAIL
>boxplot(left.bulb~habitat, data=spid.gen)
>boxplot(left.bulb/right.bulb~habitat, data=spid.gen)
```

b. Scatter Plotting

EXAMPLE:

```
>plot(left.bulb~right.bulb, data=spid.gen)
>plot(left.bulb~right.bulb, data=spid.gen, pch=as.character(habitat))
```

c. Linear Regression

EXAMPLE:

```
>gen.reg <- lm(left.bulb~right.bulb, data=spid.gen)
>gen.reg
>summary(gen.reg)
>plot(left.bulb~right.bulb, data=spid.gen)
>abline(gen.reg)
>plot(left.bulb~right.bulb, data=spid.gen, pch=as.character(habitat))
>abline(gen.reg)
```

d. Correlation Coefficient & Spearman Rank Correlation

EXAMPLE:

```
>cor.gen <- with(spid.gen, cor(left.bulb, right.bulb))
>cor.gen^2
>spear.cor.gen <- with(spid.gen, cor(left.bulb, right.bulb, method="spearman"))#to rank non-linear
>spear.cor.gen
```

e. Residuals

EXAMPLE:

```
>residuals(gen.reg)
```

f. Transformations

EXAMPLE:

```
>plot(left.bulb^2~right.bulb, data=spid.gen)
>abline(lm(left.bulb^2~right.bulb, data=spid.gen))
>summary(lm(left.bulb^2~right.bulb, data=spid.gen))
>with(spid.gen, identify(left.bulb^2~right.bulb, labels=habitat))#identify with clicks
```

g. Mention splines, glm, etc.

2. Comparing Discrete Treatment Effects

a. Chi Squared

EXAMPLE:

```
>obs <- c(32, 7, 13, 10, 14, 43, 105)
>exp <- c(.15, .05, .05, .05, .10, .20, .40)
>chisq.test(obs, p=exp)
```

b. T-test

EXAMPLE:

```
>with(spid.gen, t.test(left.bulb))#H0 - true mean is equal to 0; can set true mean with mu=
>habiat.t2 <- t.test(left.bulb~habitat, data=spid.gen)
>habitat.tless <- t.test(left.bulb~habitat, data=spid.gen, alternative="less")
>habitat.tgreater <- t.test(left.bulb~habitat, data=spid.gen, alternative="greater")
```

c. ANOVA

EXAMPLE:

```
>library(car)
>gen.lm <- lm(left.bulb~habitat, data=spid.gen)
>anova(gen.lm)
>gen.anova <- aov(left.bulb~habitat, data=spid.gen)
>summary(gen.anova)
```

d. ANCOVA

EXAMPLE:

```
>plot(left.bulb~right.bulb, data=spid.gen, pch=as.character(habitat))
>gen.ancova <- lm(left.bulb~habitat*carapace.length, data=spid.gen)
>summary(gen.ancova)
```

3. Principal Components Analysis

EXAMPLE:

```
>sp.matrix <- with(spid.gen, cbind(left.bulb, right.bulb, carapace.length, leg4.length))
>sp.matrix
>sp.pca <- princomp(sp.matrix, cor=TRUE)
>summary(sp.pca)
>loadings(sp.pca)
>biplot(sp.pca)
>sp.pca$scores
```

### 2019_09_24

### IV. Graphing Options

#### A. Adding points, lines, titles, & legends

1. abline() & points()

EXAMPLE:

```
> plot(left.bulb~right.bulb, data=spid.gen, col="white", main="Left palpal bulb vs. right palpal bulb", xlab="Right Palpal Bulb Length (cm)", ylab="Left Palpal Bulb Length (cm)")
> points(left.bulb~right.bulb, data=spid.gen[ which(spid.gen$habitat=="A"), ], col="red")
> points(left.bulb~right.bulb, data=spid.gen[ which(spid.gen$habitat=="B"), ], col="green", pch=22, cex=4)
> abline(gen.reg, col="purple", cex=8, lty=4)
> legend("topleft", c("left", "right"), col=c("red", "green"), pch=c(1, 22))
```

### V. Saving Output

#### A. Copy and paste log into text file and save.

#### B. Saving graphs

EXAMPLE:

```
> pdf(file= "/Users/michaelbrewer/Desktop/BulbReg.pdf")
> plot(left.bulb~right.bulb, data=spid.gen, col="white", main="Left palpal bulb vs. right palpal bulb", xlab="Right Palpal Bulb Length (cm)", ylab="Left Palpal Bulb Length (cm)")
> points(left.bulb~right.bulb, data=read.csv("/Users/michaelbrewer/Dropbox/R-Workshop/spider_A.csv"), col="red")
> points(left.bulb~right.bulb, data=read.csv("/Users/michaelbrewer/Dropbox/R-Workshop/spider_B.csv"), col="green", pch=22, cex=4)
> abline(gen.reg, col="purple", cex=8, lty=4)
> legend("topleft", c("left", "right"), col=c("red", "green"), pch=c(1, 22))
> dev.off()
```

### VI. R plotting

```
> plot(left.bulb~right.bulb, data=spid.gen, col="white", main="Left palpal bulb vs. right palpal bulb", xlab="Right Palpal Bulb Length (cm)", ylab="Left Palpal Bulb Length (cm)") # Plot scatter plot with white (invisible) points
> points(left.bulb~right.bulb, data=read.csv("/Users/michaelbrewer/Dropbox/Public/spider_A.csv"), col="red")
> points(left.bulb~right.bulb, data=read.csv("/Users/michaelbrewer/Dropbox/Public/spider_B.csv"), col="green", pch=19, cex=4)
> abline(gen.reg, col="purple", lty=6)
> legend("topleft", c("A", "B"), col=c("red", "green"), pch=c(1, 19))
> dev.copy("spider_gen.pdf")
> dev.off()
```

```
> plot(1:25, rep(0.25,25), pch=1:25, col=1:25, ylim=c(0,6), cex=2, ylab="Line types (lty) 1 to 6", xlab="Plotting character (pch) 1 to 25 and colours (col) 1 to 8", main="Line types (lty), plotting characters (pch) and colours (col) for plot and xyplot", lab=c(25,7,2))
> points(1:8, rep(0.5,8), pch=20, col=1:8, cex=4)
> abline(h=1:6, lty=1:6)
```

### Looping in R

```
x <- 5
if(x > 0){
	print("Positive number")
}

x <- -5
if(x < 0){
	print("Positive number")
} else {
	print("Negative number")
}

x <- 0
if (x < 0) {
   print("Negative number")
} else if (x > 0) {
   print("Positive number")
} else
   print("Zero")

> a = c(5,7,2,9)
> ifelse(a %% 2 == 0,"even","odd")

x <- c(2,5,3,9,8,11,6)
count <- 0
for (val in x) {
    if(val %% 2 == 0)  count = count+1
}
print(count)

i <- 1
while (i < 6) {
   print(i)
   i = i+1
}

x <- 1:5
for (val in x) {
    if (val == 3){
        break
    }
    print(val)
}

x <- 1:5
for (val in x) {
    if (val == 3){
        next
    }
    print(val)
}

x <- 1
repeat {
   print(x)
   x = x+1
   if (x == 6){
       break
   }
}
```

### Matrix Manipulations

```
> B = matrix(
+   c(2, 4, 3, 1, 5, 7),
+   nrow=3,
+   ncol=2)
> B             # B has 3 rows and 2 columns

> t(B)          # transpose of B

> C = matrix(
+   c(7, 4, 2),
+   nrow=3,
+   ncol=1)
> C             # C has 3 rows
> cbind(B, C)

> c(B)
```

### Null Models

```
#rnorm(n,mean,sd)
#sample(x, size, replace = FALSE, prob = NULL)

>rnorm(100,50,10) -> poop
>hist(poop)
>sample(poop, 10)
>test <- 1:100
>sample(test, 50, replace=F)
>sample(test, 100, replace=T)
```


### Paralellization in R

A. lapply library

```
lapply(1:3, function(x) c(x, x^2, x^3))
lapply(1:3/3, round, digits=3)
```

B. Parallel library

```
library(parallel)
```
Calculate the number of cores

```
no_cores <- detectCores() - 1
```

Initiate cluster

```
cl <- makeCluster(no_cores)

parLapply(cl, 2:4,
          function(exponent)
            2^exponent)

stopCluster(cl)
```

### Foreach library

```
library(foreach)
library(doParallel)

cl<-makeCluster(no_cores)
registerDoParallel(cl)
base <- 2

foreach(exponent = 2:4,
        .combine = c)  %dopar%
  base^exponent

foreach(exponent = 2:4,
	      .combine = rbind)  %dopar%
	base^exponent

foreach(exponent = 2:4,
        .combine = list,
        .multicombine = TRUE)  %dopar%
  base^exponent

stopImplicitCluster()
```

### 2019_09_26

#### Group activity

```
wget -O Brewer2012PlosONE.zip http://bit.ly/Brewer2012PLoSONE
unzip Brewer2012PlosONE.zip
```

Bash stuff... cat mac.txt | tr "\r" "\n" > unix.txt

```
$cat Colobognatha-TAX.csv Julida-taxa.csv Polydesmida-taxa.csv | cut -d"," -f2,3,4 > temp.csv
$mkdir goodfiles
$cp *.csv goodfiles
$cd goodfiles
$rm Colobognatha-TAX.csv Julida-taxa.csv Polydesmida-taxa.csv
$cat *csv > TaxDist.csv
$grep -v "^genus,family,order" TaxDist.csv | sed "s/genus,family,order//g" | sed "s/\\r/\\n/g" | grep -v "^$" | sed "s/ //g" > TaxDist.csv2
```

R stuff

```
> data <- read.csv("TaxDist.csv2", header=F, quote="")
> plot(data$sp~data$gen, col=data$tax, pch=as.numeric(data$tax)+15, cex=2, xlab="Number of genera", ylab="Number of species")
> sp_gen_reg <- lm(data$sp~data$gen)
> abline(sp_gen_reg, lty=2)
> summary(sp_gen_reg)
> sp_gen_anova <- lm(data$sp/data$gen~data$tax)
> anova(sp_gen_anova)
```

### ggplot advanced plotting pcfb_examples
**See r-graph-gallery.com**

#### Violin Plots

```
#install.packages("vioplot")
library(vioplot)
```

```
with(spid.gen, vioplot(left.bulb[habitat=="A"], left.bulb[habitat=="B"], col=rgb(0.1,0.4,0.7,0.7), names=c("A","B")))
```

#### Ridgeline (Joy) Plot

```
#install.packages("ggplot2")
#install.packages("ggridges")

library(ggridges)
library(ggplot2)
```

```
ggplot(spid.gen, aes(x=left.bulb/carapace.length, y=habitat, fill=habitat)) +
  geom_density_ridges() +
  theme_ridges() +
  theme(legend.position = "none")
```

#### Spider (Radar) Chart

```
#install.packages("fmsb")
library(fmsb)
```

```
set.seed(99)
data=as.data.frame(matrix( sample( 0:20 , 15 , replace=F) , ncol=5))
colnames(data)=c("math" , "english" , "biology" , "music" , "R-coding" )
rownames(data)=paste("mister" , letters[1:3] , sep="-")

data=rbind(rep(20,5) , rep(0,5) , data)

radarchart(data)
```

```
colors_border=c( rgb(0.2,0.5,0.5,0.9), rgb(0.8,0.2,0.5,0.9) , rgb(0.7,0.5,0.1,0.9) )
colors_in=c( rgb(0.2,0.5,0.5,0.4), rgb(0.8,0.2,0.5,0.4) , rgb(0.7,0.5,0.1,0.4) )
radarchart( data  , axistype=1 ,
    #custom polygon
    pcol=colors_border , pfcol=colors_in , plwd=4 , plty=1,
    #custom the grid
    cglcol="grey", cglty=1, axislabcol="grey", caxislabels=seq(0,20,5), cglwd=0.8,
    #custom labels
    vlcex=0.8
    )
legend(x=0.7, y=1, legend = rownames(data[-c(1,2),]), bty = "n", pch=20 , col=colors_in , text.col = "grey", cex=1.2, pt.cex=3)
```

```
colors_border=c( rgb(0.2,0.5,0.5,0.9), rgb(0.8,0.2,0.5,0.9) , rgb(0.7,0.5,0.1,0.9) )
colors_in=c( rgb(0.2,0.5,0.5,0.4), rgb(0.8,0.2,0.5,0.4) , rgb(0.7,0.5,0.1,0.4) )
radarchart( data[-c(1,2),]  , axistype=0 , maxmin=F,
    #custom polygon
    pcol=colors_border , pfcol=colors_in , plwd=4 , plty=1,
    #custom the grid
    cglcol="grey", cglty=1, axislabcol="black", cglwd=0.8,
    #custom labels
    vlcex=0.8
    )
legend(x=0.7, y=1, legend = rownames(data[-c(1,2),]), bty = "n", pch=20 , col=colors_in , text.col = "grey", cex=1.2, pt.cex=3)
```

### 2019_10_01

## 5 - Code repositories and version Control

## Create repository on Bitbucket.org

## Install Git

```
sudo apt update
sudo apt install git
git clone https://michaelsbrewer@bitbucket.org/michaelsbrewer/practicalcomputing.git
cd practicalcomputing
echo "# My project's README" >> README.md
git add README.md
git commit -m "Initial commit"
git push -u origin master
cd practicalcomputing
echo "Dr. Brewer rules!" >> notes.txt
git status
git add notes.txt
git status
git commit -m 'The truth.'
git config --global user.email "michaelsbrewer@gmail.com"
git config --global user.name "Michael Brewer"
git commit -m 'The truth.'
```

### Create file in Bitbucket website

```
<p>Dr. Brewer does the following:</p>
	<p>
	<b>Rules!!!</b><br>
	</p>
```

### Pull new file

```
git pull --all
```

### Create branch and fast-forward merge files

```
git branch future-plans
git checkout future-plans
firefox DrBrewer
nano DrBrewer
<p>
        <b>Owns!!!</b><br>
</p>
firefox DrBrewer
git add DrBrewer
git commit -m "Added more of Dr. Brewer's functions."
git checkout master
git merge future-plans
git branch -D future-plans
git status
git push origin --delete future-plans
```

### Delete repository from website

```
cd ../../
rm -r repos
rm -rf repos
```

### 2019-10-22

## Creating, editing, and analyzing images/graphics

### Install Inkscape, GIMP, FIJI, and Imagemagick

### Short discussion - pixel vs. vector images

```
wget -O Brewer2014Evol_fig2_Example.zip http://bit.ly/Brewer2014Evol_fig2_Example
unzip Brewer2014Evol_fig2_Example.zip
```

### GIMP Example

#### Removing background
* Add alpha channel
* Fuzzy select tool (wand) [u]
* Feathered edges (optional) - select radius
* Adjust threshold
* Zoom in and expand the selection
* Press delete key to remove background

#### Color selection and measurements
* Select Color Picker Tool (dropper) [o]
* Select use info window [shift]

### Fiji/ImageJ Example

https://imagej.net/Category:Tutorials

#### Measurements
* Open image
* Draw line over scale bar
* Set scale
* Draw desired line
* Select measure

### 2019-10-24

### Inkscape Example

```
wget http://teacherweb.com/WQ/ElementarySchool/HawaiianIslands/hi-chain-outline.gif

wget http://bit.ly/BrachycybeImageExample
```

### Imagemagick Example

_Compiling from source_

```
$sudo apt-get install build-essential checkinstall && apt-get build-dep imagemagick -y
$sudo wget http://www.imagemagick.org/download/ImageMagick.tar.gz
$sudo tar -xzvf ImageMagick.tar.gz
$cd ImageMagick
$./configure
$make clean
$make
```

_Using brew package manager_

```
brew update
brew install imagemagick
```

#### convert to grayscale

```
convert -type Grayscale color.jpg gray.jpg
```

#### negate

```
convert black-white.png -negate white-black.png
```

#### rotate

```
convert upside-down.png -rotate 180 rightside-up.png
```

#### resize

```
convert img.gif -resize 64x64 resized_img.gif
```

#### find the differences between two images

```
composite img1.jpg img2.jpg -compose difference diff.jpg
```

#### compare images using a metric (can be mse, psnr and more)

```
compare -verbose -metric mse 1.jpg 2.jpg difference.png
```

#### compose two images together using a mask (stencil)

```
convert -size 512x256 tile:img.jpg  tile:img2.jpg mask.jpg -composite result.jpg
```
#### remove alpha

```
convert input.png -alpha off output.png
```

### 2019_10_29

### Working on remote machines

**Students need to install VPN**
Go to piratesvpn.ecu.edu

#### SSH

```
ssh guest@150.216.20.70
ssh -X guest@150.216.20.70
fastqc
touch test.txt
nautilus .
exit
```

#### Transfering files

```
cd ~/praccomp
tar -czvf scripts_MSB_20160328.tar.gz scripts
sftp guest@150.216.20.70
ls
cd /media/BigRAID
!ls
put scripts_MSB_20160328.tar.gz scripts_MSB_20160328.tar.gz
exit

scp guest@150.216.20.70:/media/BigRAID/test.txt .
```

#### Controlling programs

```
sleep 1000
ls
```

What happens?

```
[ctrl+c]
sleep 1000 &
ls
```

What happens?

```
>ps
>ps -A
>ps -A | grep sleep
>top
>q
>kill -9 [PID]
>sleep 1000
>killall sleep
>sleep 1000
>[ctrl+z]
>bg
>fg
>[ctrl+c]
```

```
>ssh guest@150.216.20.70
>screen -S test
>sleep 1000
>[ctrl+a]d
>exit
>ssh guest@150.216.20.70
>screen -ls
>screen -r test
>[ctrl+a]k
>nohup sleep 1000 &
>exit
>ssh guest@150.216.20.70
```

#### Finding your IP address

```
host $HOSTNAME
ifconfig
```

#### Connect to your own machine
```
ssh manager@localhost
```

### Compiling software

#### From apt-get repositories

```
sudo apt-get update
sudo apt-get install htop
sudo apt update
sudo apt install screen
```

#### From source code

```
wget https://github.com/stamatak/standard-RAxML/archive/master.zip
unzip master.zip
ls
cd raxml
make -f Makefile.gcc
```

### 2019_10_31

:jack_o_lantern:

### Parallelization in Bash, Python, and R
#### Bash Parallelization

```
screen -S parallel
parallel -j 4 sleep {} ::: 1000 1000 1000 1000 1000
ctrl+a
k
y
```

#### R Parallelization
A. lapply library

```
lapply(1:3, function(x) c(x, x^2, x^3))
lapply(1:3/3, round, digits=3)
```

B. Parallel library

```
library(parallel)
```
Calculate the number of cores

```
no_cores <- detectCores() - 1
```

Initiate cluster

```
cl <- makeCluster(no_cores)

parLapply(cl, 2:4,
          function(exponent)
            2^exponent)

stopCluster(cl)
```

C. Foreach library

```
install.packages(c("foreach", "doParallel"))
library(foreach)
library(doParallel)

cl<-makeCluster(no_cores)
registerDoParallel(cl)
base <- 2

foreach(exponent = 2:4,
        .combine = c)  %dopar%
  base^exponent

foreach(exponent = 2:4,
	      .combine = rbind)  %dopar%
	base^exponent

foreach(exponent = 2:4,
        .combine = list,
        .multicombine = TRUE)  %dopar%
  base^exponent

stopImplicitCluster()
```
